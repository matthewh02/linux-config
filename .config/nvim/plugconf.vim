"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ------------------- Plugin-specific Configuration ---------------------------
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" --- NERDTree
    "autocmd vimenter * NERDTree                " Uncomment to autostart NERDTree
    let NERDTreeShowLineNumbers=1
    let NERDTreeShowHidden=1
    let g:NERDTreeWinSize=38
    " Exit of NERDTree is the only window
    autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" --- Vimtex
    let g:tex_flavor='latex'
    let g:vimtex_view_method='zathura'
" --- tex-conceal.vim
    set conceallevel=2
    let g:tex_conceal='abdgms'

" --- UltiSnips
    let g:UltiSnipsExpandTrigger = "<S-tab>"
    let g:UltiSnipsJumpForwardTrigger = "<c-j>"
    let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
    let g:UltiSnipsEditSplit = 'context'
 
" --- Neomake
    "call neomake#configure#automake('nrwi',500)
    "let g:neomake_open_list=2

" --- Autopairs
    "autocmd Filetype tex,vim let b:AutoPairs = {}

" --- Markdown-preview
    let g:mkdp_auto_close=1                    " Close preview after leaving md buffer
    let g:mkdp_browser='firefox'

" --- vim-tmux-navigator
    let g:tmux_navigator_save_on_switch=2       " Write all buffers before switch

" --- Lightline - with coc.nvim configuration
  let g:lightline = {
	\ 'colorscheme': 'powerline',
	\ 'active': {
	\   'left': [ [ 'mode', 'paste' ],
	\             [ 'cocstatus', 'readonly', 'filename', 'modified' ] ]
	\ },
	\ 'component_function': {
	\   'cocstatus': 'coc#status'
	\ },
	\ }

" ------ Coc.nvim configuration (adapted from the README) ------
"  --- Install coc extensions
    let g:coc_global_extensions = [
        \ 'coc-vimtex',
        \ 'coc-clangd',
        \ 'coc-json',
        \ 'coc-python'
        \]

set hidden              " Text Edit may fail if hidden is not set
set updatetime=200      " For better user experience (defualt 4000ms)
set shortmess+=c        " Don't pass messages to |ins-completion-menu|
set signcolumn=yes      " Always show signcolumn
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif


" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')


