"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" --------------------------- General Configuration---------------------------
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" --- General Settings ---
    set nocompatible                    " vIMproved
    set encoding=utf-8
    set clipboard+=unnamedplus          " Enable use of system clipboard
    set noswapfile
    filetype plugin on
    syntax on
    set spelllang=en

    set scrolljump=5                    " Lines to scroll when cursor leaves screen
    set scrolloff=3                     " Start scrolling 3 lines from edge
    
    " Sane comments: no auto-comment on new line
    autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" --- Interface ---
    set cmdheight=1                     " Size of command bar
    set noshowmode                      " Don't show mode below status bar (lightline!)
    set cursorline                     " Line at current position
    set splitright splitbelow           " Sensible split options...
    
    set linebreak
" --- Persistent undo ---
    " Set directory for undo files
    let target_path = expand('~/.config/nvim/persistent-undo/')
    " Create directory if non-existent
    if !isdirectory(target_path)
        call system('mkdir -p' . target_path)
    endif
    " Tell NeoVim where undo directory is
    let &undodir = target_path
    " Set persistent undo
    set undofile

" --- Line Numbers ---
    set number                          " Number column
    " Switch between relative numbers upon focus change
    " Relative when focused; normal when unfocused
    augroup numbertoggle 
        autocmd!
        autocmd BufEnter,FocusGained * set relativenumber
        autocmd BufLeave,FocusLost * set norelativenumber
    augroup END

" --- Colours ---
    set termguicolors                   " Enable true colours
    let g:gruvbox_italics=1             " Enable italics in theme
    colorscheme gruvbox

" --- Search ---
    set incsearch                       " Search as you type
    set hlsearch                        " Highlight matches
    set ignorecase                      " Ignore case in searching

" --- Spaces and Tabs ---
    set tabstop=4                       " Number of visual spaces per tab
    set softtabstop=4                   " Number of spaces in tab when editing
    set expandtab                       " Tabs are spaces
    set smarttab                        " Tabs are smart
    set autoindent
    set shiftwidth=4

" --- terminal ---
    " start terminal in insert mode
    au BufEnter * if &buftype == 'terminal' | :startinsert | endif
    function! OpenTerminal()
        split term://bash
        resize 10
    endfunction

" --- Other ---
    let g:python3_host_prog = '/bin/python3'
