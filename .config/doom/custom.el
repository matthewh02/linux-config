(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   '("/storage/Home/Knowledge/Zettelkasten/cam/ib_biogeography_presentation.org" "/storage/Home/Knowledge/Zettelkasten/cam/geography_ia_refs.org" "/storage/Home/Knowledge/Zettelkasten/cam/quaternary_refs.org" "/storage/Home/Knowledge/Zettelkasten/wiki/cryosphere.org" "/storage/Home/Knowledge/Zettelkasten/wiki/biogeography_refs.org"))
 '(warning-suppress-types '((:warning))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
