;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)
(setq doom-theme 'doom-nord)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

(setq-default delete-by-moving-to-trash t
              tab-width 4
              undo-limit 80000000               ; Set undo to 80MB
              evil-want-fine-undo t             ; Not just the last insert block!
              auto-save-default t
              truncate-string-ellipsis "↷"
              process-connection-type nil      ;To make xdg-open work: see https://askubuntu.com/a/824123
              user-full-name "Matthew Harris" )

(display-time-mode 1)

(setq doom-font (font-spec :family "JetBrainsMono Nerd Font Mono" :weight 'medium))

;; (use-package! helm
;;   :config
;;   (setq case-fold-search t
;;         helm-case-fold-search nil
;;         )
;;   )

(map! :leader
      (:prefix ("f" . "file")
       :desc "Save all org buffers open" "o" #'org-save-all-org-buffers
       ))
(use-package! org
  :config
  (progn
    (require 'ox-latex)
    (setq
     ;; Make things look nice
     org-hide-emphasis-markers t
     org-pretty-entities t
     org-ellipsis "↷"

     org-fontify-whole-heading-line t
     org-fontify-done-headline t
     org-fontify-quote-and-verse-blocks t

     org-image-actual-width '(500)      ; https://lists.gnu.org/archive/html/emacs-orgmode/2012-08/msg01388.html

     ;; (font-lock-add-keywords 'org-mode
     ;;                        '(("^ *\\([-]\\) "
     ;;                           (0 (prog1 () (compose-region (match-beginning 1)
     ;;                                                        (match-end 1) "•"))))))

     ;; Startup (i.e., when org file opened)
     org-startup-indented t
     org-startup-with-inline-images t
     org-startup-folded "overview"

     ;; Other
     org-list-allow-alphabetical t
     ;; Org id
     org-id-ts-format "D-%Y-%m-%d-T-%H.%M.%S.%6N" ; TS format for below
     org-id-method 'ts            ; Use timestap for org id; useful for org roam

    )
  )
)

(after! org
  (setq
   ;; LaTeX
   org-latex-prefer-user-labels t
   org-latex-pdf-process '("latexmk -pdf -f")
   org-id-track-globally t

   org-preview-latex-default-process 'imagemagick
   org-latex-default-packages-alist '(("AUTO" "inputenc" t ("pdflatex"))
                                      ("T1" "fontenc" t ("pdflatex"))
                                      ("" "graphicx" t)
                                      ("" "grffile" t)
                                      ("" "longtable" nil)
                                      ("" "wrapfig" nil)
                                      ("" "rotating" nil)
                                      ("normalem" "ulem" t)
                                      ("" "amsmath" t)
                                      ("" "textcomp" t)
                                      ("" "amssymb" t)
                                      ("" "capt-of" nil))

   org-latex-packages-alist '(
                              ("" "mh-style" t)
                              ("" "temp-bibsettings" t)
                              ("colorlinks=true,linkcolor=blue,urlcolor=cyan" "hyperref" nil)
                              )


   ;; Export
   org-export-with-tags nil
   )
  (add-to-list 'org-latex-classes
               '("mh-book" "\\documentclass[12pt, openany, a4paper]{book}"
                 ("\\chapter{%s}" . "\\chapter*{%s}")
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
                 )
               )

  )

;; Babel
(after! org
  (setq
   org-src-fontify-natively t
   )
  (org-babel-do-load-languages
   'org-babel-load-languages
   '(
     (R . t)
     (python . t)
     (yaml . t)
     )
   )
  )

(setq mh-home "/storage/Home/"
      knowledge-dir (concat mh-home "Knowledge/")
      org-roam-directory (concat knowledge-dir "Zettelkasten/")
      pdf-dirs '("/storage/Home/Library" "/storage/Zotero/storage") ; All PDFs from zotero
      deft-directory org-roam-directory

      org-directory (concat mh-home ".org/") ; Needs to be set
      org-id-locations-file (expand-file-name ".orgids" org-directory)
      org-default-notes-file (expand-file-name "notes.org" org-directory)
      )

(after! org-roam
  (cl-defmethod org-roam-node-doom-filetitle ((node org-roam-node)) ; Required for 'org-roam-node-doom-hierarchy
    "Return the value of \"#+title:\" (if any) from file that NODE resides in.
If there's no file-level title in the file, return empty string."
    (or (if (= (org-roam-node-level node) 0)
            (org-roam-node-title node)
          (org-roam-node-file-title node))
        ""))

  (cl-defmethod org-roam-node-doom-hierarchy ((node org-roam-node))
    "Return hierarchy for NODE, constructed of its file title and direct title.
     If some elements are missing, they will be stripped out.

    EDITS: removed OLP [outline path]"
    (let ((title     (org-roam-node-title node))
          (level     (org-roam-node-level node))
          (filetitle (org-roam-node-doom-filetitle node))
          (separator (propertize org-eldoc-breadcrumb-separator 'face 'shadow)))
      (cl-case level
        ;; node is a top-level file
        (0 filetitle)
        ;; node is a heading
        (t (concat (propertize filetitle 'face '(shadow italic))
                   separator title))
        )))
  )

(use-package! org-roam
  :after org
  :init
  ;; Doom-specific: from doom's roam2.el
  (doom-load-packages-incrementally
   '(ansi-color dash f rx seq magit-section emacsql emacsql-sqlite))

  (setq org-roam-v2-ack t) ; Don't display warning message dedicated for v1 users. Need to be set early.

  :config
  ;; == Note ==
  ;; Doom's roam2.el uses a specific function to setup org roam but delay database initialisation. Look into this
  (setq org-roam-db-location "/storage/Home/Knowledge/Zettelkasten/org-roam.db")
  (org-roam-db-autosync-mode)   ; Ensure org-roam is available at startup
  (add-hook 'org-roam-mode-hook #'turn-on-visual-line-mode) ; From doom's roam2.el
  (setq
   org-roam-node-display-template ; Adapted from doom's roam2.el; methods defined above
   (format "${doom-hierarchy:*} %s"
           (propertize "${tags:10}" 'face 'org-tag)
           )
   )
  )

(setq org-roam-node-display-template
      (concat "${doom-hierarchy:*} ${tags:10}" (propertize "${tags:10}" 'face 'org-tag)))

(after! org-roam
  (map! :leader
        :prefix ("r" . "org-roam")
        "f" #'org-roam-node-find
        "F" #'org-roam-ref-find
        "i" #'org-roam-node-insert
        "r" #'org-roam-buffer-toggle
        :desc "Full search" "s" #'mh/org-roam-rg-search
        (:prefix ("n" . "node properties")
                 "a" #'org-roam-alias-add
                 "A" #'org-roam-alias-remove
                 "t" #'org-roam-tag-add
                 "T" #'org-roam-tag-remove
                 "r" #'mh/org-roam-ref-add
                 "R" #'org-roam-ref-remove)
        (:prefix ("b" . "bib")
         "a" #'orb-note-actions
         :desc "Helm-bibtex" "b" #'helm-bibtex
         :desc "Org-Ref-helm-cite" "c" #'org-ref-cite-insert-helm
         "p" #'org-ref-paren-cite)
        (:prefix ("a" . "admin")
                 "i" #'org-roam-update-org-id-locations
                 "d" #'org-roam-db-sync)
        )
  )

(after! org
  (setq org-roam-db-node-include-function ; Exclude headlines with :attach: from org-db see Manual
        (lambda ()
          (not (member "ATTACH" (org-get-tags)))))

  (org-roam-bibtex-mode +1)       ; https://org-roam.discourse.group/t/minimal-setup-for-helm-bibtex-org-roam-v2-org-roam-bibtex/1971/2

  (setq org-roam-capture-everywhere t)
  )

(setq org-roam-capture-templates
      '(
        ;; Literature Notes Templates
        ("l" "lit" plain
         (file "~/.config/doom/roam-templates/lit.org")
         :if-new (file+head "literature/${citekey}.org" ":properties:\n:created: %u\n:author: Matthew Harris\n:end:\n#+title: ${author-or-editor-abbrev}, (${date}). `${title}'\n")
         :unnarrowed t
         :immediate-finish t
         :jump-to-captured t)
        ;; Longer notes
        ("w" "wiki" plain          ; i.e., longer notes
         (file "~/.config/doom/roam-templates/default.org")
         :if-new (file+head "wiki/${slug}.org" ":properties:\n:created: %u\n:author: Matthew Harris\n:end:\n#+title: ${title}\n#+filetags: :wiki:")
         :unnarrowed t
         :immediate-finish t)

        ;; Longer notes for 'blog'
        ("h" "hugo" plain          ; i.e., longer notes
         (file "~/.config/doom/roam-templates/blog.org")
         :if-new (file+head "hugo/${slug}.org" ":properties:\n:created: %u\n:author: Matthew Harris\n:end:\n#+title: ${title}\n#+filetags: :blog:")
         :unnarrowed t
         :immediate-finish t)
        ;; Personal notes
        ("p" "personal" plain          ; i.e., personal (not synced)
         (file "~/.config/doom/roam-templates/default.org")
         :if-new (file+head "personal/${slug}.org" ":properties:\n:created: %u\n:author: Matthew Harris\n:end:\n#+title: ${title}\n#+filetags: :personal:")
         :unnarrowed t
         :immediate-finish t)
        ;; Concept notes (or specific notes) that contain specific information
        ("s" "concept" plain
           (file "~/.config/doom/roam-templates/default.org")
           :if-new (file+head "concept/${slug}.org" ":properties:\n:created: %u\n:author: Matthew Harris\n:end:\n#+title: ${title}\n#+filetags: :concept:")
           :unnarrowed t
           :immediate-finish t)
        ))

(use-package! org-roam-bibtex
  :after org-roam
  :config
  (require 'org-ref)
  (setq orb-process-file-keyword t
        orb-file-field-extensions '("pdf")
        orb-preformat-keywords '("citekey" "date" "entry-type" "pdf?" "note?" "author" "editor" "author-abbrev" "editor-abbrev" "author-or-editor-abbrev" "url" "title" "author-or-editor" "year") ; All but last two were       defaults
        orb-roam-ref-format 'org-ref-v3
        )
  )

(use-package! helm-bibtex
  :config
  (setq
   bibtex-completion-bibliography '("/storage/Home/Knowledge/Zettelkasten/Library.bib") ; See org-ref readme
   bibtex-completion-library-path pdf-dirs ; See org-ref and helm-bibtex readme
   bibtex-completion-pdf-field 'nil
   ;"File"                        ; See README
   )
  )

(use-package! org-ref
  :after org
  :init
  (require 'org-ref-helm)
  ;; Define function to allow org-ref citations within ox-hugo export
  ;; https://ox-hugo.scripter.co/doc/org-ref-citations/
  (with-eval-after-load 'ox
    (defun my/org-ref-process-buffer--html (backend)
      "Preprocess `org-ref' citations to HTML format.

Do this only if the export backend is `html' or a derivative of
that."
      ;; `ox-hugo' is derived indirectly from `ox-html'.
      ;; ox-hugo <- ox-blackfriday <- ox-md <- ox-html
      (when (org-export-derived-backend-p backend 'html)
        (org-ref-process-buffer 'html)))
    (add-to-list 'org-export-before-parsing-hook #'my/org-ref-process-buffer--html))
  :config
  (setq
   ;; Some variables from the README are set above in helm-bibtex
   bibtex-completion-notes-path "/storage/Home/Knowledge/Zettelkasten/literature" ; See org-ref readme
   bibtex-completion-cite-default-command "textcite"
   org-ref-default-citation-link "textcite"
   bibtex-completion-pdf-open-function 'org-open-file
   ;; See README - set up for Helm
   org-ref-insert-link-function 'org-ref-insert-link-hydra/body
   org-ref-insert-cite-function 'org-ref-cite-insert-helm
   org-ref-insert-label-function 'org-ref-insert-label-link
   org-ref-insert-ref-function 'org-ref-insert-ref-link
   org-ref-cite-onclick-function (lambda (_) (org-ref-citation-hydra/body))
   ;; See README
   bibtex-completion-pdf-open-function
   (lambda (fpath)
     (call-process "open" nil 0 nil fpath))
   )

  ;; Provide second interactive function to auto include "cite:&"
  (defun mh/org-roam-ref-add (ref)
    "Add REF to the node at point."
    (interactive "sRef: ")
    (let ((node (org-roam-node-at-point 'assert)))
      (save-excursion
        (goto-char (org-roam-node-point node))
        (org-roam-property-add "ROAM_REFS" (if (memq " " (string-to-list ref))
                                               (concat "\"" ref "\"")
                                             (concat "cite:&" ref))))))
  )

(defun mh/org-roam-rg-search ()
  "Search org-roam directory using consult-ripgrep. With live-preview."
  (interactive)
  (let ((consult-ripgrep-command "rg --null --ignore-case --type org --line-buffered --color=always --max-columns=500 --no-heading --line-number . -e ARG OPTS"))
    (consult-ripgrep org-roam-directory)))
(global-set-key (kbd "C-c rr") 'mh/org-roam-rg-search)

(use-package! org-transclusion
  :after org
  :init
  (map!
   :leader
   :prefix "r"
   (:prefix ("t" . "org-transclusion")
   :desc "OT Make from Link" "l" #'org-transclusion-make-from-link
   :desc "Org Transclusion Mode" "t" #'org-transclusion-mode
   :desc "OT add (create)" "c" #'org-transclusion-add
   :desc "OT activate" "a" #'org-transclusion-activate
   :desc "OT deactivate" "d" #'org-transclusion-deactivate
   :desc "OT refresh" "r" #'org-transclusion-refresh
   ))
  :config
  (advice-remove 'org-link-search '+org--recenter-after-follow-link-a)
  (add-to-list 'org-transclusion-exclude-elements 'keyword)
  )

(use-package! websocket
    :after org)

(use-package! org-roam-ui
    :after org
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

(defun my/org-roam-git-sync ()
    (interactive)
    (shell-command "bash /storage/Home/Knowledge/Zettelkasten/zettel-git-auto.sh"))
(map! :leader
      (:prefix "r"
      :desc "Git sync zettel directory" "p" #'my/org-roam-git-sync)
      )

(with-eval-after-load 'ox
  (require 'ox-hugo))

(after! ox-hugo (defun org-hugo--tag-processing-fn-remove-tags-maybe (tags-list info)
                  "Remove user-specified tags/categories.
See `org-hugo-tag-processing-functions' for more info."
                  ;; Use tag/category string (including @ prefix) exactly as used in Org file.
                  (let ((tags-categories-to-be-removed '("wiki"))) ;"my_tag" "@my_cat"
                    (cl-remove-if (lambda (tag_or_cat)
                                    (member tag_or_cat tags-categories-to-be-removed))
                                  tags-list)))
  (add-to-list 'org-hugo-tag-processing-functions
               #'org-hugo--tag-processing-fn-remove-tags-maybe)
  )

;; (with-eval-after-load 'treemacs
;;   (defun treemacs-ignore-object (file _)
;;     ((string-suffix-p ".o" file))

;;                 (add-to-list 'treemacs-ignored-file-predicates #'treemacs-ignore-object))

(use-package! treemacs
  :init
  (map! :map global-map
        :leader
        :desc "Treemacs"
        "f t" #'treemacs-toggle
        ))

(use-package! lsp
  :config
  (setq lsp-pylsp-plugins-pyflakes-enabled t
        lsp-pylsp-plugins-pylint-enabled t
        lsp-pylsp-plugins-rope-completion-enabled t
        lsp-pylsp-plugins-yapf-enabled t)
  )

(after! org
  (add-hook 'org-mode-hook
            (lambda ()
              (mapc (lambda (pair) (push pair prettify-symbols-alist))
                    '(
                      ;; ("\\rightarrow" . ?→)
                      ("\\rightarrow" . ?)
                      ("\\leftarrow" . ?←)
                      ("\\uparrow" . ?↑)
                      ("\\downarrow" . ?↓)
                      ("\\implies" . ?⇒)
                      ))
              )))

(load "ess-autoloads")
(after! ess
  (setq inferior-R-args "--no-save")    ; This is *necessary* as otherwise R has a fatal error
  (setq ess-eval-empty t)               ; don't skip non-code lines
  (setq ess-ask-for-ess-directory nil)
  (map! "M-m" 'mh/insert_pipe_native_R_operator)
  )

(defun mh/insert_pipe_native_R_operator ()
  "R - |> native pipe operator"
  (interactive)
  (just-one-space 1)
  (insert "|>")
  (reindent-then-newline-and-indent))

;; prepare the arguments
(setq dotfiles-git-dir (concat "--git-dir=" (expand-file-name "~/.cfg")))
(setq dotfiles-work-tree (concat "--work-tree=" (expand-file-name "~")))
;; function to start magit on dotfiles
(defun dotfiles-magit-status ()
  (interactive)
  (add-to-list 'magit-git-global-arguments dotfiles-git-dir)
  (add-to-list 'magit-git-global-arguments dotfiles-work-tree)
  (call-interactively 'magit-status))
(map! :leader
      :prefix "r"
      "g" #'dotfiles-magit-status)
;; wrapper to remove additional args before starting magit
(defun magit-status-with-removed-dotfiles-args ()
  (interactive)
  (setq magit-git-global-arguments (remove dotfiles-git-dir magit-git-global-arguments))
  (setq magit-git-global-arguments (remove dotfiles-work-tree magit-git-global-arguments))
  (call-interactively 'magit-status))
;; redirect global magit hotkey to our wrapper
(global-set-key (kbd "C-x g") 'magit-status-with-removed-dotfiles-args)
(map! :leader
      :prefix "g"
      "g" #'magit-status-with-removed-dotfiles-args)
