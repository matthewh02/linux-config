#!/bin/bash
############################################################
#  __  __ _   _
# |  \/  | | | |    Copyright (c) 2023 Matthew Harris
# | |\/| | |_| |
# | |  | |  _  |    https:/www.gitlab.com/matthewh02
# |_|  |_|_| |_|
#
############################################################
# Adapted from ArcoLinux Initial Config

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

# Second monitor
# xrandr --output HDMI-A-0 --mode 1920x1080 --right-of eDP --brightness 0.8
# Warmer screen
redshift -P -O 3500 # Make screen less blue light
# Disable Touchscreen (annoying me)
xinput disable "ELAN2514:00 04F3:29CF"

# Set wallpaper
~/.core.config/scripts/.fehbg &
# Start Conky
(conky -c $HOME/.conkyrc) &

# Start sxhkd to replace Qtile-native key-bindings
run sxhkd -c ~/.config/sxhkd/sxhkdrc

# Start utility applications
run nm-applet &
run xfce4-power-manager &
numlockx on &
run flameshot &
picom --config $HOME/.config/qtile/scripts/picom.conf &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &

emacs --daemon &
onedrive --monitor &

## Start other applications at start
run pasystray &
run blueman-applet &
run nextcloud &
run keepassxc &
#run syncthing-gtk &
# run protonvpn &
