######################################################################
#  __    __   __    __
# |  \  /  | | |  | |   Copyright (c) 2022 Matthew Harris
# |*| \/ |*| |*|__|*|
# |*|    |*| |* __ *|   https:/www.gitlab.com/matthewharris02
# |*|    |*| |*|  |*|
# |_|    |_| |_|  |_|   Qtile Configuration
#
######################################################################

############################################################
# This is the original copyright notice
# ==========================================================
# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ==========================================================

### == Imports == ###

import os
import re
import socket
import subprocess
from typing import List  # noqa: F401
from libqtile import layout, bar, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen, Rule, EzKey
from libqtile.lazy import lazy
from libqtile.widget import Spacer
import arcobattery

### == Set defaults to avoid repeating == ##
MOD = "mod4"
CTRL = "control"
home = "/home/matthew/"
TERM = "kitty"
EDITOR = "emacsclient -c -a emacs"

################################################################################
#### ==== ArcoLinux Qtile Config ==== ####
################################################################################
mod = "mod4"                    # Super key: primary modifier
mod1 = "alt"
mod2 = "control"

keys = [
    # Important keys
    EzKey("M-q", lazy.window.kill()),
    EzKey("M-C-r", lazy.restart()),
    EzKey("M-f", lazy.window.toggle_fullscreen()),

    # Shift window focus
    EzKey("M-h", lazy.layout.left()),
    EzKey("M-l", lazy.layout.right()),
    EzKey("M-j", lazy.layout.down()),
    EzKey("M-k", lazy.layout.up()),
    EzKey("M-<space>", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Layout keys
    EzKey("M-n", lazy.layout.normalize()),
    EzKey("M-<Tab>", lazy.layout_next()),
    EzKey("M-C-f", lazy.layout.flip()),

    # Multi monitor
    EzKey("M-<period>", lazy.to_screen(0)), # Keyboard focus to Laptop
    EzKey("M-<slash>", lazy.to_screen(1)), # Keyboard focus to Monitor

### == ArcoLinux Key bindings == ###

# RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),


# FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "control"], "f", lazy.layout.flip()),

# FLIP LAYOUT FOR BSP
    Key([mod, "mod1"], "k", lazy.layout.flip_up()),
    Key([mod, "mod1"], "j", lazy.layout.flip_down()),
    Key([mod, "mod1"], "l", lazy.layout.flip_right()),
    Key([mod, "mod1"], "h", lazy.layout.flip_left()),

# MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),

# MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    # Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    # Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    # Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    # Key([mod, "shift"], "Right", lazy.layout.swap_right()),
    Key([mod,"shift"], "space", lazy.window.toggle_floating()),
   ]

# ==== Groups ====

#           Name: label
my_groups = {"1": [""],
             "2": ["2"],
             "3": ["3"],
             "4": ["4"],
             "5": ["5"],
             "6": ["6"],
             "7": [""],
             "8": ["8"],
             "9": ["9"],
             "0": ["0"]
             }
groups = []
for (name,optns) in my_groups.items():
    groups.append(
        Group(name = name,
              label = optns[0],
              layout = "monadtall")
    )

for i in groups:
    keys.extend([

#CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod], "Tab", lazy.screen.next_group()),
        Key([mod, "shift" ], "Tab", lazy.screen.prev_group()),
        Key(["mod1"], "Tab", lazy.screen.next_group()),
        Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        Key([mod, "control"], i.name, lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])


colours_nord = {"dark1":    ["#2E3440", "#2E3440"], # nord0
                "dark2":    ["#3B4252", "#3B4252"], # nord1
                "dark3":    ["#434c5e", "#434c5e"], # nord2
                "dark4":    ["#4c566a", "#4c566a"], # nord3
                "light1":   ["#d8dee9", "#d8dee9"], # nord4
                "light2":   ["#e5e9f0", "#e5e9f0"], # nord5
                "light3":   ["#eceff4", "#eceff4"], # nord6
                "blue1":    ["#8fbcbb", "#8fbcbb"], # nord7
                "blue2":    ["#88c0d0", "#88c0d0"], # nord8
                "blue3":    ["#81a1c1", "#81a1c1"], # nord9
                "blue4":    ["#5e81ac", "#5e81ac"], # nord10
                "red":      ["#bf616a", "#bf616a"], # nord11
                "orange":   ["#d08770", "#d08770"], # nord12
                "yellow":   ["#ebcb8b" ,"#ebcb8b"], # nord13
                "green":    ["#a3be8c", "#a3be8c"], # nord14
                "purple":   ["#b48ead", "#b48ead"]} # nord15



def init_layout_theme():
    return {"margin":5,
            "border_width":2,
            "border_focus": colours_nord["blue2"][1],
            "border_normal": colours_nord["blue4"][1]
            }

layout_theme = init_layout_theme()


layouts = [
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Matrix(**layout_theme),
    layout.Bsp(**layout_theme),
    layout.Floating(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Max(**layout_theme)
]

# COLORS FOR THE BAR
#Theme name : ArcoLinux Default
def init_colors():
    return [["#2F343F", "#2F343F"],  # color 0
            ["#2F343F", "#2F343F"],  # color 1
            ["#c0c5ce", "#c0c5ce"],  # color 2
            ["#fba922", "#fba922"],  # color 3
            ["#3384d0", "#3384d0"],  # color 4
            ["#f3f4f5", "#f3f4f5"],  # color 5
            ["#cd1f3f", "#cd1f3f"],  # color 6
            ["#62FF00", "#62FF00"],  # color 7
            ["#6790eb", "#6790eb"],  # color 8
            ["#a9a9a9", "#a9a9a9"]]  # color 9


colors = colours_nord


# WIDGETS FOR THE BAR

def init_widgets_defaults():
    return dict(font="Tinos Nerd Font",
                fontsize=12,
                padding=0,
                background=colors["dark1"])


widget_defaults = init_widgets_defaults()


def init_widgets_list():
    widgets_list = [
            widget.GroupBox(
                font="Tinos Nerd Font",
                fontsize=16,
                margin_y=-1,
                margin_x=0,
                padding_y=6,
                padding_x=5,
                borderwidth=0,
                disable_drag=True,
                active=colors["blue4"],
                inactive=colors["dark4"],
                rounded=False,
                highlight_method="line",
                this_current_screen_border=colors["light3"],
                this_screen_border=colors["red"],
                other_screen_border=colors["red"],
                other_current_screen_border=colors["purple"],
                foreground=colors["light1"],
                background=colors["dark1"]
                ),
            widget.Sep(
                linewidth=1,
                padding=10,
                foreground=colors["light1"],
                background=colors["dark1"]
                ),
            widget.CurrentLayout(
                font="Noto Sans Bold",
                foreground=colors["light1"],
                background=colors["dark1"]
                ),
            widget.Sep(
                linewidth=1,
                padding=10,
                foreground=colors["light1"],
                background=colors["dark1"]
                ),
            widget.WindowName(
                font="Tinos Nerd Font",
                fontsize=14,
                foreground=colors["light1"],
                background=colors["dark1"],
                ),
            widget.TextBox(
                font="Noto Sans",
                text='',
                foreground=colors["dark4"],
                background=colors["dark1"],
                fontsize=30
                ),
            arcobattery.BatteryIcon(
                padding=0,
                scale=0.7,
                y_poss=2,
                theme_path=home + "/.config/qtile/icons/battery_icons_horiz",
                update_interval=10,
                background=colors["dark4"],
                ),
            widget.Battery(
                font="Noto Sans",
                update_interval=10,
                fontsize=14,
                foreground=colors["light1"],
                background=colors["dark4"],
                charge_char="",
                discharge_char="",
                format="{char} {percent:2.0%}",
                padding = 5
                ),
            widget.TextBox(
                font="Noto Sans",
                text='',
                foreground=colors["dark1"],
                background=colors["dark4"],
                fontsize=30
                ),
            widget.Clock(
                font="Noto Sans Nerd Font",
                foreground=colors["blue1"],
                background=colors["dark1"],
                fontsize=14,
                format=" %A %d %B %Y  %H:%M (%Z)",
                padding = 5
                ),
            widget.TextBox(
                font="Noto Sans",
                text='',
                foreground=colors["dark4"],
                background=colors["dark1"],
                fontsize=30
                ),
            widget.Systray(
                background=colors["dark4"],
                icon_size=20,
                padding=0
                ),
            ]
    return widgets_list


widgets_list = init_widgets_list()

widgets_screen1 = init_widgets_list()
widgets_screen2 = init_widgets_list()

screens = [Screen(top=bar.Bar(widgets=widgets_screen1, size=26, opacity=0.8))]
           # Screen(top=bar.Bar(widgets=widgets_screen2, size=26, opacity=0.8), background=colors["light1"])]


# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size())
]

dgroups_key_binder = None
dgroups_app_rules = []

# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# BEGIN

#########################################################
################ assgin apps to groups ##################
#########################################################
# @hook.subscribe.client_new
# def assign_app_group(client):
#     d = {}
#     #####################################################################################
#     ### Use xprop fo find  the value of WM_CLASS(STRING) -> First field is sufficient ###
#     #####################################################################################
#     d[group_names[0]] = ["Navigator", "Firefox", "Vivaldi-stable", "Vivaldi-snapshot", "Chromium", "Google-chrome", "Brave", "Brave-browser",
#               "navigator", "firefox", "vivaldi-stable", "vivaldi-snapshot", "chromium", "google-chrome", "brave", "brave-browser", ]
#     d[group_names[1]] = [ "Atom", "Subl", "Geany", "Brackets", "Code-oss", "Code", "TelegramDesktop", "Discord",
#                "atom", "subl", "geany", "brackets", "code-oss", "code", "telegramDesktop", "discord", ]
#     d[group_names[2]] = ["Inkscape", "Nomacs", "Ristretto", "Nitrogen", "Feh",
#               "inkscape", "nomacs", "ristretto", "nitrogen", "feh", ]
#     d[group_names[3]] = ["Gimp", "gimp" ]
#     d[group_names[4]] = ["Meld", "meld", "org.gnome.meld" "org.gnome.Meld" ]
#     d[group_names[5]] = ["Vlc","vlc", "Mpv", "mpv" ]
#     d[group_names[6]] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer",
#               "virtualbox manager", "virtualbox machine", "vmplayer", ]
#     d[group_names[7]] = ["Thunar", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt",
#               "thunar", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
#     d[group_names[8]] = ["Evolution", "Geary", "Mail", "Thunderbird",
#               "evolution", "geary", "mail", "thunderbird" ]
#     d[group_names[9]] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious",
#               "spotify", "pragha", "clementine", "deadbeef", "audacious" ]
#     ######################################################################################
#
# wm_class = client.window.get_wm_class()[0]
#
#     for i in range(len(d)):
#         if wm_class in list(d.values())[i]:
#             group = list(d.keys())[i]
#             client.togroup(group)
#             client.group.cmd_toscreen(toggle=False)

# END
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME



main = None


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])


@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])


@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True


floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules, 
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(wm_class='Arcolinux-welcome-app.py'),
    Match(wm_class='Arcolinux-tweak-tool.py'),
    Match(wm_class='Arcolinux-calamares-tool.py'),
    Match(wm_class='confirm'),
    Match(wm_class='dialog'),
    Match(wm_class='download'),
    Match(wm_class='error'),
    Match(wm_class='file_progress'),
    Match(wm_class='notification'),
    Match(wm_class='splash'),
    Match(wm_class='toolbar'),
    Match(wm_class='Arandr'),
    Match(wm_class='feh'),
    Match(wm_class='Galculator'),
    Match(wm_class='arcolinux-logout'),
    Match(wm_class='xfce4-terminal'),
    Match(title='Quick Format Citation'),  # Zotero

],  fullscreen_border_width=0, border_width=0)
auto_fullscreen = True

focus_on_window_activation = "focus"  # or smart

wmname = "LG3D"
